/**
 * 
 */
package test.common.distributed.algorithm;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import redis.clients.jedis.ShardedJedisPool;

import com.common.distributed.algorithm.model.AlgorithmModel;
import com.common.distributed.algorithm.redis.mapping.ISearchDistributedIndex;
import com.common.distributed.algorithm.redis.mapping.impl.SearchDistributedIndexImpl;

/**
 * @author liubing
 *
 */
public class TestSearchDistributedIndexImpl {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<AlgorithmModel> algorithmModels=new ArrayList<AlgorithmModel>();
		int nodeCount = 4;
		for(int i=0;i<nodeCount;i++){
			AlgorithmModel algorithmModel=new AlgorithmModel();
			algorithmModel.setHost("192.168.1."+i);
			algorithmModel.setWeight(1);
			algorithmModels.add(algorithmModel);
		}
		ApplicationContext applicationContext = new ClassPathXmlApplicationContext("spring.xml");
		ShardedJedisPool jedisPool=(ShardedJedisPool) applicationContext.getBean("shardedJedisPool");
		ISearchDistributedIndex searchDistributedIndex=new SearchDistributedIndexImpl(algorithmModels);
		searchDistributedIndex.setShardedJedisPool(jedisPool);
		for(int j=1;j<=25;j++){
			System.out.println(searchDistributedIndex.getResultByKey("liubingfileCount"+j)+",次数:"+j);
			//log.info(searchDistributedIndex.getResultByKey("liubingfileCount"+i)+",次数:"+i);
		}

	}

}
