/**
 * 
 */
package com.common.distributed.algorithm.redis.mapping;

import java.util.List;

import com.common.distributed.algorithm.model.AlgorithmModel;

import redis.clients.jedis.ShardedJedisPool;

/**
 * @author liubing
 *
 */
public interface ISearchDistributedIndex {
	/**
	 * 根据key获取结果集
	 * 如果redis没有，从分布式算法中获取；如果redis有数据，从redis获取
	 * @param key
	 * @return
	 */
	public AlgorithmModel getResultByKey(String key);
	/**
	 * 存储数据
	 * @param key
	 * @param result
	 * @return
	 */
	 public boolean saveMappingData(String key,String result);
	 /**
	  * 存取值
	  * @param shardedJedisPool
	  */
	 public void setShardedJedisPool(ShardedJedisPool shardedJedisPool);
	 /**
	  * 获取服务器对应的列表
	  * @param server
	  * @param start
	  * @param end
	  * @return
	  */
	 public List<AlgorithmModel> getKeyList(String server,long start,long end);
	 /**
	  * 删除
	  * @param key
	  */
	 public void delByKey(String key);
	 /**
	  * 根据key 获取服务器列表
	  * @param key
	  * @return
	  */
	 public AlgorithmModel getServer(String key);
}
