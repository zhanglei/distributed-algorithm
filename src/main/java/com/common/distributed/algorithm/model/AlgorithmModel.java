/**
 * 
 */
package com.common.distributed.algorithm.model;

import java.io.Serializable;

/**
 * @author liubing
 *  分布式计算 model
 */
public class AlgorithmModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4079785650907096715L;
	
	private String host;//主机名
	
	private int weight;//权重
	
	private String name;//机器名

	/**
	 * @return the host
	 */
	public String getHost() {
		return host;
	}

	/**
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * @return the weight
	 */
	public int getWeight() {
		return weight;
	}

	/**
	 * @param weight the weight to set
	 */
	public void setWeight(int weight) {
		this.weight = weight;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AlgorithmModel [host=" + host + ", weight=" + weight
				+ ", name=" + name + "]";
	}
}
