/**
 * 
 */
package com.common.distributed.algorithm.model;

import java.io.Serializable;

/**
 * @author liubing
 *分区权重参数类
 */
@SuppressWarnings("serial")
public class PartitionWeightParameter implements Serializable{
	
	 private String partition;
     private int weight;
     
     public PartitionWeightParameter(){
    	 
     }
     public PartitionWeightParameter(String partition, int weight) {
         this.partition = partition;
         this.weight = weight;
     }

     public String getPartition() {
         return partition;
     }

     public int getWeight() {
         return weight;
     }
}
